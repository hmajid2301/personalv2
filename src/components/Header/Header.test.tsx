import React from 'react';
import { render } from 'test-utils';

import { Logo } from '~/components/Logo';
import Header from './Header';

describe('<Header />', () => {
  describe('Default Header', () => {
    test('Render with default nav bar', () => {
      const links = ['Link1', 'Link2', 'Link3'];
      const { getAllByText } = render(
        <Header logo={<div>Logo</div>} links={links} />
      );

      links.forEach(link => {
        const linkElement = getAllByText(link)[0] as HTMLAnchorElement;
        expect(linkElement.href).toBe(`http://localhost/${link}`);
        expect(linkElement.className).toBe(
          `text-lg my-2 lg:text-sm lg:mx-6 lg:my-0 font-semibold tracking-wide transition duration-300
      pb-1 border-b-2 border-transparent hover:border-blue-500 hocus:text-blue-500 hover:text-blue-500`
        );
      });
    });
  });

  describe('Props: Hover Color', () => {
    test.each([
      [
        'gray-500',
        'hover:border-gray-500 hocus:text-gray-500 hover:text-gray-500',
      ],
      [
        'purple-500',
        'hover:border-purple-500 hocus:text-purple-500 hover:text-purple-500',
      ],
      ['black', 'hover:border-black hocus:text-black hover:text-black'],
    ])(
      'Render with %i hover color',
      (hoverColor: string, expectedClass: string) => {
        const links = ['Link1', 'Link2', 'Link3'];
        const { getAllByText } = render(
          <Header
            logo={<div>Logo</div>}
            links={links}
            hoverColor={hoverColor}
          />
        );

        links.forEach(link => {
          const linkElement = getAllByText(link)[0];
          expect(linkElement.className).toContain(expectedClass);
        });
      }
    );
  });

  describe('Props: Logo', () => {
    test.each([
      [<div>Haseeb</div>, 'Haseeb'],
      [
        <Logo
          accent="gray-500"
          color="black"
          hoverColor="purple-500"
          text="xyz.io"
        />,
        'xyz.io',
      ],
    ])(
      'Render with %i logo',
      (logoComp: React.ReactNode, searchString: string) => {
        const links = ['Link1', 'Link2', 'Link3'];
        const { getAllByText } = render(
          <Header logo={logoComp} links={links} />
        );

        const logos = getAllByText(searchString);
        logos.forEach(logo => {
          expect(logo).toBeTruthy();
          const logoParent = logo.parentElement as HTMLAnchorElement;
          expect(logoParent.href).toBe('http://localhost/');
        });
      }
    );
  });

  describe('Props: Links', () => {
    test.each([[['Link1', 'Link2']], [['Blog', 'Pricing', 'Product']]])(
      'Render with %i logo',
      (links: string[]) => {
        const { getAllByText } = render(
          <Header logo={<div>Haseeb</div>} links={links} />
        );

        links.forEach(link => {
          const linkElement = getAllByText(link)[0] as HTMLAnchorElement;
          expect(linkElement.href).toBe(`http://localhost/${link}`);
        });
      }
    );
  });
});
