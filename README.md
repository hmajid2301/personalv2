# Pages

Personal portfolio website hosted on `Netlify` built using Gatsby .

> Info: Currently in the process of being migrated to a Gatsby website (V3).

## Usage

To run the website locally do the following;

```bash
git clone https://gitlab.com/hmajid2301/hmajid2301.gitlab.io.git
cd hmajid2301.gitlab.io
yarn install
yarn develop
```

## Technologies Used

- Gatsby
- Storybook
- TailwindCSS
- EmotionJS
- Jest

## Previous Versions

- [V1](https://v1.haseebmajid.dev)
- [V2](https://v2.haseebmajid.dev)

## Appendix

- Built using this template [by duncanleung](https://github.com/duncanleung/gatsby-typescript-emotion-storybook)

### Inspired By

- [Treact](https://treact.owaiskhan.me)