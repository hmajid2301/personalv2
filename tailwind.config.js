module.exports = {
  theme: {
    extend: {
      colors: {
        blue: {
          '900': '#15325d',
          '800': '#204b8b',
          '700': '#1763d5',
          '600': '#1f6fe6',
          '500': '#367ee9',
          '400': '#4d8dec',
          '300': '#649cee',
          '200': '#7baaf1',
          '100': '#c2d8f8',
        },
      },
      fontFamily: {
        header: ['Inter'],
      },
    },
  },
  variants: {},
  plugins: [],
};
